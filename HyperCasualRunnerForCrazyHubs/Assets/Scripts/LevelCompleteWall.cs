﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelCompleteWall : MonoBehaviour
{
    [SerializeField] PlayerManager playerManager;
    [SerializeField] private GameObject levelCompleteWall;
    [SerializeField] private GameObject scorePanel;
    
    [SerializeField] private TextMeshProUGUI scoreTxt;
    
    private int aliveHumanNum = 0;
    private void Start()
    {
        scoreTxt.text = "";
    }
    private void Update()
    {
        scoreTxt.text = "" + aliveHumanNum * 5;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("CollectedObj"))
        {
            
            playerManager.levelState = PlayerManager.LevelState.Finished;
            aliveHumanNum += 1;
            scorePanel.SetActive(true);
        }
        
    }
}
