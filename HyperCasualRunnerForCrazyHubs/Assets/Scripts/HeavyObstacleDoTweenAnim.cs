﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HeavyObstacleDoTweenAnim : MonoBehaviour
{
    [SerializeField] private Transform obstacleTransform;
    [SerializeField] private Ease obstacleAnimEase;

    void Start()
    {
        obstacleTransform.DOMoveY(1f, 1f).SetEase(obstacleAnimEase).SetLoops(-1, LoopType.Yoyo);
    }


}
