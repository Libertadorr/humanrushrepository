﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FistDOTweenAnim1 : MonoBehaviour
{
    [SerializeField] private Transform fistTransform;
    [SerializeField] private Ease fistAnimEase;

    void Start()
    {
        fistTransform.DOMoveX(1f, 1f).SetEase(fistAnimEase).SetLoops(-1, LoopType.Yoyo);
    }

}
