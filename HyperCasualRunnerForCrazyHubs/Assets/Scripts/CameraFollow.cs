﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] PlayerManager playerManager;
    public Transform target;

    [SerializeField] float smoothSpeed;
   // private Vector3 offset = new Vector3(0, 10, 0);
    

    void LateUpdate()
    {
        
     //   offset = offset + new Vector3(0, 0, GameSettings.Instance().ZOffset); 
        if(playerManager.levelState== PlayerManager.LevelState.NotFinished) {
            Vector3 desiredPos= target.position+ GameSettings.Instance().Offset;
            Vector3 smoothedPos = Vector3.Lerp (transform.position, desiredPos, smoothSpeed);
            transform.position = new Vector3(transform.position.x, transform.position.y, smoothedPos.z);
        }
    }
}
