﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class DebugMenu : MonoBehaviour
{
    [SerializeField] private Text MovementSpeedText;
    [SerializeField] private Text ControlSpeedText;
    [SerializeField] private Text ZOffsetText;
    [SerializeField] private Text MusicVolumeText;
    [SerializeField] private Dropdown dd;
    
    void Start()
    {
        GameSettings.Instance().MovementSpeed = 10;
        MovementSpeedText.text = "Movement Speed (Default 10): " + GameSettings.Instance().MovementSpeed;
        GameSettings.Instance().ControlSpeed = 10;
        ControlSpeedText.text = "Swipe Speed (Default 10): " + GameSettings.Instance().ControlSpeed;
        GameSettings.Instance().Offset = new Vector3(0, 10, 0) + new Vector3 (0, 0, -34);
        ZOffsetText.text = "Camera Z Offset (Default -34): " + GameSettings.Instance().Offset.z;
        MusicVolumeText.text = "Music Volume (Default 1): " + 1;
        
    }

    

    public void SetMovementSpeed(float MSpeed){
        GameSettings.Instance().MovementSpeed = MSpeed;
        MovementSpeedText.text = "Movement Speed (Default 10): " + GameSettings.Instance().MovementSpeed;
    }

    public void SetControlSpeed(float CSpeed){
        GameSettings.Instance().ControlSpeed = CSpeed;
        ControlSpeedText.text = "Swipe Speed (Default 10): " + GameSettings.Instance().ControlSpeed;
    }

    public void SetCameraZOffset(float zOffset){
     
        GameSettings.Instance().Offset = new Vector3(0, 10, 0) + new Vector3 (0, 0, zOffset);
        ZOffsetText.text = "Camera Z Offset (Default -30): " + GameSettings.Instance().Offset.z;
    }

    public void SetVolume(float volume){
        MusicVolumeText.text = "Music Volume (Default 1): " + volume;
        GameSettings.SetVolume(volume);
    }

    public void changeMusic(int value){
        GameSettings.Instance().HandleDropdownData(value);
    }

}
