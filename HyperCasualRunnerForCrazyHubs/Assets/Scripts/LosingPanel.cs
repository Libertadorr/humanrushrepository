﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LosingPanel : MonoBehaviour
{
   [SerializeField] PlayerManager playerManager;

    [SerializeField] private GameObject losingPanel;

   
    void Start()
    {
        
    }

   
    void Update()
    {
        if(playerManager.collidedList.Count == 0)
        {
            playerManager.levelState = PlayerManager.LevelState.Finished;
            losingPanel.SetActive(true);
        }
    }
}
