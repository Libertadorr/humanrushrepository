﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{
    private static GameSettings _instance;
    [SerializeField, ReadOnly] private float _movementSpeed;
    [SerializeField, ReadOnly] private float _controlSpeed;
    [SerializeField, ReadOnly] private Vector3 _Offset;
    [SerializeField] AudioClip audioData1;
    [SerializeField] AudioClip audioData2;
    public static AudioSource audio;


    public void HandleDropdownData(int value)
    {
        if(value == 0)
        {
            audio.clip= audioData1;
            audio.Play();
        }

        if(value == 1)
        {
            audio.clip= audioData2;
            audio.Play();
        }
    }
    public static void SetVolume(float volume){
        audio.volume = volume;
    }

    void Start() 
    { 
        audio=gameObject.GetComponent<AudioSource>();
        HandleDropdownData(0);
    }

    public Vector3 Offset
    {
        get {return _Offset; }
        set {_Offset = value;}
    }


    public float ControlSpeed 
    {
        get {return _controlSpeed; }
        set {_controlSpeed = value;}
    }

    public float MovementSpeed 
    {
        get {return _movementSpeed; }
        set {_movementSpeed = value;}
    }

    public static GameSettings Instance(){
        if (_instance == null){
            _instance = GameObject.FindObjectOfType<GameSettings>();
        }
            return _instance;
    }
}
